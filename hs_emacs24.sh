#!/bin/sh
sudo sh -c "echo 'deb http://http.debian.net/debian wheezy-backports main' >> /etc/apt/sources.list"
sudo apt-get update 
sudo apt-get -t wheezy-backports install emacs24-nox -y
sudo apt-get install install-info
mkdir ~/.emacs.d
mkdir ~/.emacs.d/snippets
cat << 'EOF' > ~/.emacs
(load "~/.emacs.d/hs-loadpackages.el")
(global-linum-mode 1)
(require 'python-django)
EOF
cat << 'EOF' > ~/.emacs.d/hs-loadpackages.el
;hs-loadpackages.el
; loading package
(load "~/.emacs.d/hs-packages.el")

(require 'magit)
(define-key global-map (kbd "C-c m") 'magit-status)

(require 'yasnippet)
(yas-global-mode 1)
(yas-load-directory "~/.emacs.d/snippets")
(add-hook 'term-mode-hook (lambda()
    (setq yas-dont-activate t)))
EOF
cat << 'EOF' > ~/.emacs.d/hs-packages.el
(require 'cl)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(package-initialize)

(defvar required-packages
  '(
    magit
    yasnippet
    erlang
    drupal-mode
    lua-mode
    elixir-mode
  ) "a list of packages to ensure are installed at launch.")

; method to check if all packages are installed
(defun packages-installed-p ()
  (loop for p in required-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

; if not all packages are installed, check one by one and install the missing ones.
(unless (packages-installed-p)
  ; check for new packages (package versions)
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ; install the missing packages
  (dolist (p required-packages)
    (when (not (package-installed-p p))
      (package-install p))))
EOF
#run with \curl -sSL https://gist.github.com/samrose/c0fb0b5f3bcf890a6f4a/raw |bash
